package com.vigo.chartdemo;


import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@SuppressLint("ViewConstructor")
public class CustomMarkerView extends MarkerView {

    private final TextView tvContent;
    private String name;

    public CustomMarkerView(Context context, int layoutResource, String name) {
        super(context, layoutResource);

        this.name = name;
        tvContent = findViewById(R.id.tvContent);
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        if (e instanceof CandleEntry) {

            CandleEntry ce = (CandleEntry) e;

            tvContent.setText("name1 : "+name+"\n"+Utils.formatNumber(ce.getHigh(), 0, true));
        } else {

            tvContent.setText(name+"\nx: "+formatYAxis(e.getX())+",\ny: "+Utils.formatNumber(e.getY(), 0, true));
        }
        tvContent.invalidate();
        super.refreshContent(e, highlight);
    }

    private String formatYAxis(float value) {
        SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM HH:mm", Locale.ENGLISH);

        long millis = TimeUnit.HOURS.toMillis((long) value);
        return mFormat.format(new Date(millis));
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
